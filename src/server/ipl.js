//problem 1 - calculating the matches played per year
module.exports.matchesPlayedPerYear = function(matches){

    //storing the result in an object with keys as years and value as the total matches played that particular year

    return matches.reduce(function(accumulator, match){

        if(accumulator[match.season] === undefined){
            accumulator[match.season] = 1;
        }else{
            accumulator[match.season] += 1;
        }
        return accumulator;
    }, {});

}


//problem 2 - calculating the matches won by per team per year
module.exports.matchesWonPerTeamPerYear = function(matches){

    //filling the matchesWonPerTeamCount object
    return matches.reduce(function(accumulator, match) {

        if(match.winner){
            let winningTeam = match.winner;
            let season = match.season;

            if(accumulator[winningTeam]){
                if(accumulator[winningTeam][season]){
                    accumulator[winningTeam][season] += 1;
                }else{
                    accumulator[winningTeam][season] = 1;
                }
            }else{
                accumulator[winningTeam] = {};
                accumulator[winningTeam][season] = 1;
            }
        }

        return accumulator;
        
    }, {});
}


//problem 3 - calculating the extra runs conceded per team at a given season
module.exports.extraRunsPerTeam = function(matches, deliveries, season){

    //filtering out the matches played at the given season
    const matchesAtGivenSeason = matches.filter(function(match){
        return match.season == season;
    });

    const matchesIDsAtGivenSeason = matchesAtGivenSeason.reduce(function(accumulator, match){
        accumulator[match.id] = match;
        return accumulator;
    }, {});


    //filtering out the deliveries data for that given season
    const deliveriesAtGivenSeason = deliveries.filter(function(delivery){
        return matchesIDsAtGivenSeason[delivery.match_id];
    });
    

    return deliveriesAtGivenSeason.reduce(function(accumulator, delivery){
        if(accumulator[delivery.bowling_team] === undefined){
            accumulator[delivery.bowling_team] = 0;  
        }else{
            accumulator[delivery.bowling_team] += parseInt(delivery.extra_runs);
        }

        return accumulator;
    }, {});
}


//problem 4 - calculating the top 10 economic bowlers at a given season
module.exports.top10EconomicalBowlers = function(matches, deliveries, season){

    //filtering out the matches played at the given season
    const matchesAtGivenSeason = matches.filter(function(match){
        return match.season == season;
    });

    const matchesIDsAtGivenSeason = matchesAtGivenSeason.reduce(function(accumulator, match){
        accumulator[match.id] = match;
        return accumulator;
    }, {});

    //filling up the list of bowlers for the season
    const bowlers = [];

    deliveries.forEach(function (delivery) {

        if(matchesIDsAtGivenSeason[delivery.match_id]){

            if(!bowlers.find(({name}) => name === delivery.bowler)){
                const bowler = {};
                bowler.name = delivery.bowler;
                bowler.runs = parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs) + parseInt(delivery.batsman_runs);
                bowler.balls = 1;
                //calculating the economy per over
                bowler['economy'] = (bowler['runs'] / bowler['balls']) * 6;
                bowlers.push(bowler);
            }else{
                const bowler = bowlers.find(({name}) => name === delivery.bowler);
                bowler.runs += parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs) + parseInt(delivery.batsman_runs);
                bowler.balls += 1;
                //calculating the economy per over
                bowler.economy = (bowler['runs'] / bowler['balls']) * 6;
            }
        }
    });

    //sorting the list and returning the 1st 10 entries
    
    return bowlers.sort((a,b) => a.economy - b.economy).slice(0, 10);
}

